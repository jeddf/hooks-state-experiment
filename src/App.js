import React, { Component } from "react";
import ToDos from "./ToDos-Toolbox/ToDos";

class App extends Component {
  render() {
    return (
      <div className="App">
        <h2> TODOs </h2>
        <ToDos />
      </div>
    );
  }
}

export default App;
