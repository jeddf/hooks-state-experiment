import React, { memo } from "react";

const AddButton = ({ add }) => <button onClick={add}>Add</button>;

export default memo(AddButton);
