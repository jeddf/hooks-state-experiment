import React, { memo } from "react";

const ToDoInput = ({ updateValue, value }) => (
  <input
    type="text"
    value={value}
    onChange={({ target }) => updateValue(target.value)}
  />
);

export default memo(ToDoInput);
