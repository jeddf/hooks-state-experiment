import React, { memo } from "react";

const ToDo = ({
  toDo: { id, done, label },
  remove,
  markAsDone,
  markAsNotDone
}) => (
  <li key={id}>
    <input
      type="checkbox"
      checked={done}
      onChange={({ target }) =>
        target.checked ? markAsDone(id) : markAsNotDone(id)
      }
      label={label}
    />
    <span className={done ? "done" : ""}>{label}</span>
    <button onClick={() => remove(id)}>X</button>
  </li>
);

export default memo(ToDo);
