import React from "react";

const ToDoList = ({ children }) => (
  <div className="toDo-list">
    <ul>{children}</ul>
  </div>
);

export default ToDoList;
