import { useState } from "react";

const getActions = (
  { toDos, nextToDoId, newToDoLabel },
  { setToDos, setNextToDoId, setNewToDoLabel }
) => ({
  reset: toDos => {
    setToDos(toDos);
    setNextToDoId(toDos.length);
  },
  add: () => {
    setToDos([
      ...toDos,
      {
        id: nextToDoId,
        label: newToDoLabel,
        done: false
      }
    ]);
    setNextToDoId(nextToDoId + 1);
    setNewToDoLabel("");
  },
  remove: id => {
    setToDos(toDos.filter(toDo => toDo.id !== id));
  },
  markAsDone: id =>
    setToDos(
      toDos.map(toDo => (toDo.id === id ? { ...toDo, done: true } : toDo))
    ),
  markAsNotDone: id =>
    setToDos(
      toDos.map(toDo => (toDo.id === id ? { ...toDo, done: false } : toDo))
    ),
  updateLabel: setNewToDoLabel
});

const useToDos = () => {
  const [toDos, setToDos] = useState([]);
  const [nextToDoId, setNextToDoId] = useState(0);
  const [newToDoLabel, setNewToDoLabel] = useState("");
  const state = { toDos, nextToDoId, newToDoLabel };
  const actions = getActions(state, {
    setToDos,
    setNextToDoId,
    setNewToDoLabel
  });
  return [state, actions];
};

export default useToDos;
