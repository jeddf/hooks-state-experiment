import { useReducer, useMemo } from "react";

const initialState = { toDos: null, nextToDoId: 0, newToDoLabel: "" };

function reducer(state, action) {
  switch (action.type) {
    case "reset":
      return {
        ...state,
        toDos: action.payload,
        nextToDoId: action.payload.length
      };
    case "add":
      return {
        toDos: [
          ...state.toDos,
          {
            id: state.nextToDoId,
            label: state.newToDoLabel,
            done: false
          }
        ],
        nextToDoId: state.nextToDoId + 1,
        newToDoLabel: ""
      };
    case "remove":
      return {
        ...state,
        toDos: state.toDos.filter(toDo => toDo.id !== action.payload)
      };
    case "markAsDone":
      return {
        ...state,
        toDos: state.toDos.map(toDo =>
          toDo.id === action.payload ? { ...toDo, done: true } : toDo
        )
      };
    case "markAsNotDone":
      return {
        ...state,
        toDos: state.toDos.map(toDo =>
          toDo.id === action.payload ? { ...toDo, done: false } : toDo
        )
      };
    case "updateLabel":
      return {
        ...state,
        newToDoLabel: action.payload
      };
    default:
      return state;
  }
}

const getActions = dispatch => ({
  reset: data => dispatch({ type: "reset", payload: data }),
  add: () => dispatch({ type: "add" }),
  remove: id => dispatch({ type: "remove", payload: id }),
  markAsDone: id => dispatch({ type: "markAsDone", payload: id }),
  markAsNotDone: id => dispatch({ type: "markAsNotDone", payload: id }),
  updateLabel: label => dispatch({ type: "updateLabel", payload: label })
});

const useToDos = () => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const actions = useMemo(() => getActions(dispatch), [dispatch]);
  return [state, actions];
};

export default useToDos;
