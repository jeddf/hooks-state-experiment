import React, { useEffect } from "react";
import axios from "axios";

import {
  useToDos,
} from "./Hooks";

import {
  ToDo,
  ToDoList,
  ToDoInput,
  AddButton
} from "./Components";

const ToDos = () => {
  const [
    { toDos, newToDoLabel },
    { reset, add, remove, markAsDone, markAsNotDone, updateLabel }
  ] = useToDos();

  useEffect(() => {
    axios
      .get(
        "https://gist.githubusercontent.com/witalewski/fc8f043d53a0d505f84c5ddb04ae76ea/raw/7c505bbc1675a0bc8a067f8b633b531c769bb64c/data.json"
      )
      .then(({ data }) => reset(data));
  }, []);

  return toDos ? (
    <ToDoList>
      {toDos.map(toDo => (
        <ToDo
          toDo={toDo}
          key={toDo.id}
          remove={remove}
          markAsDone={markAsDone}
          markAsNotDone={markAsNotDone}
        />
      ))}
      <div className="new-toDo">
        <ToDoInput value={newToDoLabel} updateValue={updateLabel} />
        <AddButton add={add} />
      </div>
    </ToDoList>
  ) : (
    <div>Loading...</div>
  );
};

export default ToDos;
